package net.qqxh.sunflow.flowclient.client;

import com.alibaba.fastjson.JSONArray;

import java.util.Map;

public interface TaskClient {
    JSONArray list(Map params);
}
